const orderMapper = require('./mappers/orderMapper');
const productMapper = require('./mappers/productMapper');
const customerMapper = require('./mappers/customerMapper');

const productsService = require('./services/productsService');

const { Customer } = require('./models/Customer');
const { Order } = require('./models/Order');
const { Product } = require('./models/Product');

module.exports = {
  aggregateOrdersByProductInDay: orderMapper.aggregateOrdersByProductInDay,
  aggregateOrdersByCustomerInDay: orderMapper.aggregateOrdersByCustomerInDay,
  findProductById: productMapper.findProductById,
  findCustomerById: customerMapper.findCustomerById,
  getTotalPriceOfProducts: productsService.getTotalPriceOfProducts,

  models: {
    Customer,
    Order,
    Product,
  },
};
