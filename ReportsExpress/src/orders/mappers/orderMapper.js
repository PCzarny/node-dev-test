const repository = require('../services/repository');
const { Order } = require('../models/Order');

function mapRowToOrder(row) {
  return new Order({
    number: row.number,
    customerId: row.customer,
    createdAt: row.createdAt,
    productsIds: row.products,
  });
}

function findOrdersByDay(day) {
  return repository.fetchOrders()
    .then((orderRows) => {
      return orderRows
        .filter((orderRow) => orderRow.createdAt === day)
        .map(mapRowToOrder);
    });
}

function group(acc, key, value) {
  const currentValue = acc.get(key) || [];

  return acc.set(key, [...currentValue, value]);
}

function groupOrdersByCustomerId(orders) {
  return orders.reduce((acc, order) => group(acc, order.customerId, order), new Map());
}

function groupOrdersByProductId(orders) {
  return orders.reduce((acc, order) => {
    return order.productsIds
      .reduce((x, product) => group(x, product, order), acc);
  }, new Map());
}

// Aggregation functions are places in mapper module, as for most of DB connectors
// most efficient way to aggregate is operation on DB.
function aggregateOrdersByCustomerInDay(day) {
  return findOrdersByDay(day)
    .then((orders) => groupOrdersByCustomerId(orders));
}

function aggregateOrdersByProductInDay(day) {
  return findOrdersByDay(day)
    .then((orders) => groupOrdersByProductId(orders));
}

module.exports = {
  aggregateOrdersByCustomerInDay,
  aggregateOrdersByProductInDay,
};
