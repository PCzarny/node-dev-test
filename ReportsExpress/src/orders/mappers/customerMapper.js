const repository = require('../services/repository');
const { Customer } = require('../models/Customer');

function mapRowToCustomer(customerRow) {
  return new Customer(customerRow);
}

function findCustomerById(id) {
  return repository.fetchCustomers()
    .then((customerRows) => customerRows.find((row) => row.id === id))
    .then((customerRow) => {
      return customerRow ? mapRowToCustomer(customerRow) : null;
    });
}

module.exports = {
  findCustomerById,
};
