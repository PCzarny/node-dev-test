const repository = require('../services/repository');
const { Product } = require('../models/Product');

function mapRowToProduct(productRow) {
  return new Product(productRow);
}

function findProductsByIds(ids) {
  return repository.fetchProducts()
    .then((productRows) => {
      return productRows
        .filter((productRow) => ids.includes(productRow.id))
        .map(mapRowToProduct);
    });
}

function findProductById(id) {
  return repository.fetchProducts()
    .then((productRows) => productRows.find((productRow) => id === productRow.id))
    .then((productRow) => {
      return productRow ? mapRowToProduct(productRow) : null;
    });
}

module.exports = {
  findProductsByIds,
  findProductById,
};
