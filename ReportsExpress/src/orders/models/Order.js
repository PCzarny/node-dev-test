class Order {
  constructor({ number, customerId, productsIds, createdAt }) {
    this.number = number;
    this.customerId = customerId;
    this.productsIds = productsIds;
    this.createdAt = createdAt;
  }
}

module.exports = { Order };
