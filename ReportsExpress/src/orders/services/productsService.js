const productMapper = require('../mappers/productMapper');

async function getTotalPriceOfProducts(productsIds) {
  const quantityPerProductId = productsIds
    .reduce((acc, productId) => {
      const currentValue = acc.get(productId) || 0;
      return acc.set(productId, currentValue + 1);
    }, new Map());

  const uniqueProductsIds = Array.from(quantityPerProductId.keys());

  const products = await productMapper.findProductsByIds(uniqueProductsIds);

  return products
    .map((product) => quantityPerProductId.get(product.id) * product.price)
    .reduce((sum, pricePerProduct) => sum + pricePerProduct, 0);
}

module.exports = {
  getTotalPriceOfProducts,
};
