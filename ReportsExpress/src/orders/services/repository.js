/* eslint-disable global-require */

function fetchProducts() {
  return new Promise((resolve) => resolve(require('../resources/data/products')));
}

function fetchOrders() {
  return new Promise((resolve) => resolve(require('../resources/data/orders')));
}

function fetchCustomers() {
  return new Promise((resolve) => resolve(require('../resources/data/customers')));
}

module.exports = {
  fetchOrders,
  fetchProducts,
  fetchCustomers,
};
