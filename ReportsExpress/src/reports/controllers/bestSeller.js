const productReports = require('../services/productReports');

function formatResponse({ product, quantity, totalPrice }) {
  if (!product) return null;

  return {
    productName: product.name,
    quantity,
    totalPrice,
  };
}

function controller(req, res, next) {
  return productReports.getBestSellerByDay(req.params.date)
    .then(formatResponse)
    .then((response) => res.status(200).json({ data: response }))
    .catch(next);
}

module.exports = controller;
