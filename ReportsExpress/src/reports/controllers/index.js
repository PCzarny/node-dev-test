const { Router } = require('express');

const bestBuyer = require('./bestBuyer');
const bestSeller = require('./bestSeller');

const router = Router();

router.get('/customer/:date', bestBuyer);
router.get('/products/:date', bestSeller);

module.exports = router;
