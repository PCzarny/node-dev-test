const customerReports = require('../services/customerReports');

function formatResponse({ customer, totalPrice }) {
  if (!customer) return null;
  return {
    customerName: customer.fullName,
    totalPrice,
  };
}

function controller(req, res, next) {
  return customerReports.getBestBuyersByDay(req.params.date)
    .then(formatResponse)
    .then((response) => res.status(200).json({ data: response }))
    .catch(next);
}

module.exports = controller;
