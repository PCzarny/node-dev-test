const orderModule = require('../../orders');

async function getTotalPriceForCustomer(customerId, orders) {
  // can be replaced with flatMap after node version update
  const productsIds = orders
    .map((order) => order.productsIds)
    .reduce((flatted, ids) => flatted.concat(...ids), []);

  const totalPrice = await orderModule.getTotalPriceOfProducts(productsIds);
  return { customerId, totalPrice };
}

function getAllTotalPricesForCustomers(ordersPerCustomer) {
  const entries = Array.from(ordersPerCustomer.entries());
  return Promise.all(entries
    .map(([customerId, orders]) => getTotalPriceForCustomer(customerId, orders)));
}

function findBestBuyer(ordersPerCustomer) {
  return getAllTotalPricesForCustomers(ordersPerCustomer)
    .then((totalPrices) => {
      let maxTotalPrice = 0;
      let bestBuyerId = null;

      totalPrices.forEach(({ customerId, totalPrice }) => {
        if (totalPrice > maxTotalPrice) {
          maxTotalPrice = totalPrice;
          bestBuyerId = customerId;
        }
      });

      return { customerId: bestBuyerId, totalPrice: maxTotalPrice };
    });
}

async function prepareResult({ customerId, totalPrice }) {
  if (!customerId) return {};

  const customer = await orderModule.findCustomerById(customerId);

  if (!customer) throw new Error(`Customer doesn't exist`);

  return { customer, totalPrice };
}

function getBestBuyersByDay(date) {
  return orderModule.aggregateOrdersByCustomerInDay(date)
    .then(findBestBuyer)
    .then(prepareResult);
}

module.exports = { getBestBuyersByDay };
