const orderModule = require('../../orders');

function countProductInOrder(productId, order) {
  return order.productsIds.filter((id) => productId === id).length;
}

function countProductInOrders(productId, orders) {
  return orders
    .map((order) => countProductInOrder(productId, order))
    .reduce((sum, productCount) => sum + productCount, 0);
}

function findBestSeller(ordersPerProduct) {
  let maxQuantity = 0;
  let bestSellerId = null;

  const entries = Array.from(ordersPerProduct.entries());
  entries.forEach(([productId, orders]) => {
    const quantity = countProductInOrders(productId, orders);

    if (quantity > maxQuantity) {
      maxQuantity = quantity;
      bestSellerId = productId;
    }
  });

  return { productId: bestSellerId, quantity: maxQuantity };
}

async function prepareResult({ productId, quantity }) {
  if (!productId) return {};

  const product = await orderModule.findProductById(productId);

  if (!product) throw new Error(`Product doesn't exist`);

  return {
    product,
    quantity,
    totalPrice: quantity * product.price,
  };
}

function getBestSellerByDay(date) {
  return orderModule.aggregateOrdersByProductInDay(date)
    .then(findBestSeller)
    .then(prepareResult);
}

module.exports = { getBestSellerByDay };
