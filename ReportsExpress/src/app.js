const express = require('express');
const cors = require('cors');
const helmet = require('helmet');

const reportRouter = require('./reports/controllers');

const app = express();

app.use(express.urlencoded({ extended: true }));

// adding Helmet to enhance your API's security
app.use(helmet());

// parse JSON bodies into JS objects
app.use(express.json());

// enabling CORS for all requests
app.use(cors());

app.use('/report', reportRouter);

app.use((req, res) => {
  res.status(404).send(`Sorry can't find that!`);
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  // eslint-disable-next-line no-console
  console.error(err.stack);
  res.status(500).send({ error: 'Something broke!' });
});

module.exports = app;
