const { stub } = require('sinon');

const { getBestBuyersByDay } = require('../../../../src/reports/services/customerReports');

const orderModule = require('../../../../src/orders');

const { Order, Customer } = orderModule.models;

describe(`customerReports`, () => {
  describe(`getBestBuyersByDay`, () => {
    beforeEach(() => {
      stub(orderModule, 'aggregateOrdersByCustomerInDay');
      stub(orderModule, 'findCustomerById');
      stub(orderModule, 'getTotalPriceOfProducts')
        .callsFake((productsIds) => {
          // Stub getTotalPriceOfProducts logic with stubed prices
          const prices = new Map([[1, 7.5], [2, 2]]);
          const totalPrice = productsIds
            .map((productId) => prices.get(productId))
            .reduce((sum, price) => sum + price, 0);
          return Promise.resolve(totalPrice);
        });
    });

    afterEach(() => {
      orderModule.aggregateOrdersByCustomerInDay.restore();
      orderModule.findCustomerById.restore();
      orderModule.getTotalPriceOfProducts.restore();
    });

    const date = `2019-08-07`;

    context(`when there is no order for specific date`, () => {
      beforeEach(() => orderModule.aggregateOrdersByCustomerInDay.resolves(new Map()));

      it(`should resolve with empty object`, () => {
        return getBestBuyersByDay(date)
          .then((bestBuyerDetails) => {
            expect(bestBuyerDetails).to.deep.equal({});
            expect(orderModule.aggregateOrdersByCustomerInDay).to.be.calledOnceWith(date);
          });
      });
    });

    context(`when there is one best buyer customer`, () => {
      const ordersPerCustomer = new Map([
        [1, [
          new Order({
            number: 'testNumber1',
            customerId: 1,
            createdAt: date,
            productsIds: [1, 2],
          }),
          new Order({
            number: 'testNumber2',
            customerId: 1,
            createdAt: date,
            productsIds: [1],
          }),
        ]],
        [2, [
          new Order({
            number: 'testNumber3',
            customerId: 2,
            createdAt: date,
            productsIds: [1, 2],
          }),
        ]],
      ]);
      beforeEach(() => orderModule.aggregateOrdersByCustomerInDay.resolves(ordersPerCustomer));

      context(`when customer found`, () => {
        const customer = new Customer({ id: 1, firstName: 'Jack', lastName: `Bar` });

        beforeEach(() => orderModule.findCustomerById.resolves(customer));

        it(`should resolve with details of customer 1`, () => {
          return getBestBuyersByDay(date)
            .then((bestBuyerDetails) => {
              expect(bestBuyerDetails).to.deep.equal({
                customer,
                totalPrice: 17,
              });
              expect(orderModule.aggregateOrdersByCustomerInDay).to.be.calledOnceWith(date);
              expect(orderModule.findCustomerById).to.be.calledOnceWith(1);

              expect(orderModule.getTotalPriceOfProducts).to.be.calledTwice;
              expect(orderModule.getTotalPriceOfProducts).to.be.calledWith([1, 2, 1]);
              expect(orderModule.getTotalPriceOfProducts).to.be.calledWith([1, 2]);
            });
        });
      });

      context(`when customer could not be found`, () => {
        beforeEach(() => orderModule.findCustomerById.resolves(null));

        it(`should reject with error`, () => {
          return getBestBuyersByDay(date)
            .then(() => expect.fail())
            .catch((error) => {
              expect(error.message).to.equal(`Customer doesn't exist`);
              expect(orderModule.aggregateOrdersByCustomerInDay).to.be.calledOnceWith(date);
              expect(orderModule.findCustomerById).to.be.calledOnceWith(1);
            });
        });
      });
    });

    context(`when there are few best buyer customers`, () => {
      const ordersPerCustomer = new Map([
        [1, [
          new Order({
            number: 'testNumber1',
            customerId: 1,
            createdAt: date,
            productsIds: [2],
          }),
          new Order({
            number: 'testNumber2',
            customerId: 1,
            createdAt: date,
            productsIds: [1],
          }),
        ]],
        [2, [
          new Order({
            number: 'testNumber3',
            customerId: 2,
            createdAt: date,
            productsIds: [1, 2],
          }),
        ]],
      ]);

      beforeEach(() => orderModule.aggregateOrdersByCustomerInDay.resolves(ordersPerCustomer));

      context(`when customer found`, () => {
        const customer = new Customer({ id: 1, firstName: '', lastName: '' });

        beforeEach(() => orderModule.findCustomerById.resolves(customer));

        it(`should resolve with details of first best buyer`, () => {
          return getBestBuyersByDay(date)
            .then((bestBuyerDetails) => {
              expect(bestBuyerDetails).to.deep.equal({
                customer,
                totalPrice: 9.5,
              });
              expect(orderModule.aggregateOrdersByCustomerInDay).to.be.calledOnceWith(date);
              expect(orderModule.findCustomerById).to.be.calledOnceWith(1);

              expect(orderModule.getTotalPriceOfProducts).to.be.calledTwice;
              expect(orderModule.getTotalPriceOfProducts).to.be.calledWith([1, 2]);
              expect(orderModule.getTotalPriceOfProducts).to.be.calledWith([1, 2]);
            });
        });
      });
    });
  });
});
