const { stub } = require('sinon');

const { getBestSellerByDay } = require('../../../../src/reports/services/productReports');

const orderModule = require('../../../../src/orders');

const { Order, Product } = orderModule.models;

describe(`productReports`, () => {
  describe(`getBestSellerByDay`, () => {
    beforeEach(() => {
      stub(orderModule, 'aggregateOrdersByProductInDay');
      stub(orderModule, 'findProductById');
    });
    afterEach(() => {
      orderModule.aggregateOrdersByProductInDay.restore();
      orderModule.findProductById.restore();
    });

    const date = `2019-08-07`;

    context(`when there is no order for specific date`, () => {
      beforeEach(() => orderModule.aggregateOrdersByProductInDay.resolves(new Map()));

      it(`should resolve with empty object`, () => {
        return getBestSellerByDay(date)
          .then((bestSellersDetails) => {
            expect(orderModule.aggregateOrdersByProductInDay).to.be.calledOnceWith(date);
            expect(bestSellersDetails).to.deep.equal({});
          });
      });
    });

    context(`when there is one best seller product`, () => {
      const ordersPerProduct = new Map([
        [1, [
          new Order({
            number: 'testNumber1',
            customerId: 1,
            createdAt: date,
            productsIds: [1, 2],
          }),
          new Order({
            number: 'testNumber2',
            customerId: 2,
            createdAt: date,
            productsIds: [1, 1],
          }),
        ]],
        [2, [
          new Order({
            number: 'testNumber1',
            customerId: 1,
            createdAt: date,
            productsIds: [1, 2],
          }),
        ]],
      ]);
      beforeEach(() => orderModule.aggregateOrdersByProductInDay.resolves(ordersPerProduct));

      context(`when product found`, () => {
        const product = new Product({ id: 1, name: 'Leather jacket', price: 25 });

        beforeEach(() => orderModule.findProductById.resolves(product));

        it(`should resolve with details of product 1`, () => {
          return getBestSellerByDay(date)
            .then((bestSellersDetails) => {
              expect(orderModule.aggregateOrdersByProductInDay).to.be.calledOnceWith(date);
              expect(bestSellersDetails).to.deep.equal({
                product,
                quantity: 3,
                totalPrice: 75,
              });
              expect(orderModule.findProductById).to.be.calledOnceWith(1);
            });
        });
      });

      context(`when product could not be found`, () => {
        beforeEach(() => orderModule.findProductById.resolves(null));

        it(`should reject with error`, () => {
          return getBestSellerByDay(date)
            .then(() => expect.fail())
            .catch((error) => {
              expect(error.message).to.equal(`Product doesn't exist`);
              expect(orderModule.aggregateOrdersByProductInDay).to.be.calledOnceWith(date);
              expect(orderModule.findProductById).to.be.calledOnceWith(1);
            });
        });
      });
    });

    context(`when there are few best seller products`, () => {
      const ordersPerProduct = new Map([
        [1, [
          new Order({
            number: 'testNumber1',
            customerId: 1,
            createdAt: date,
            productsIds: [1, 2, 2],
          }),
          new Order({
            number: 'testNumber2',
            customerId: 2,
            createdAt: date,
            productsIds: [1],
          }),
        ]],
        [2, [
          new Order({
            number: 'testNumber1',
            customerId: 1,
            createdAt: date,
            productsIds: [1, 2, 2],
          }),
        ]],
      ]);
      beforeEach(() => orderModule.aggregateOrdersByProductInDay.resolves(ordersPerProduct));

      context(`when product found`, () => {
        const product = new Product({ id: 1, name: 'Leather jacket', price: 25 });

        beforeEach(() => orderModule.findProductById.resolves(product));

        it(`should resolve with details of first best seller`, () => {
          return getBestSellerByDay(date)
            .then((bestSellersDetails) => {
              expect(bestSellersDetails).to.deep.equal({
                product,
                quantity: 2,
                totalPrice: 50,
              });
              expect(orderModule.aggregateOrdersByProductInDay).to.be.calledOnceWith(date);
              expect(orderModule.findProductById).to.be.calledOnceWith(1);
            });
        });
      });
    });
  });
});
