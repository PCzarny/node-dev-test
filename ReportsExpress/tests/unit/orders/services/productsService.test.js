const { stub } = require('sinon');

const { getTotalPriceOfProducts } = require('../../../../src/orders/services/productsService');

const productMapper = require('../../../../src/orders/mappers/productMapper');
const { Product } = require('../../../../src/orders/models/Product');

describe(`productsService`, () => {
  describe(`getTotalPriceOfProducts`, () => {
    context(`when there are 2 types of products and one of them appear twice`, () => {
      const productsIds = [1, 2, 1];
      const products = [
        new Product({ id: 1, name: 'foo', price: 7.5 }),
        new Product({ id: 2, name: 'bar', price: 2 }),
      ];

      before(() => stub(productMapper, 'findProductsByIds').resolves(products));
      afterEach(() => productMapper.findProductsByIds.resetHistory());
      after(() => productMapper.findProductsByIds.restore());


      it(`should calculate total price of 3 products`, () => {
        return getTotalPriceOfProducts(productsIds)
          .then((totalPrice) => {
            expect(totalPrice).to.equal(17);
            expect(productMapper.findProductsByIds)
              .to.be.calledOnceWith([1, 2]);
          });
      });
    });
  });
});
