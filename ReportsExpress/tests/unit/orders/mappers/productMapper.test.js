const { Product } = require('../../../../src/orders/models/Product');
const { findProductsByIds, findProductById } = require('../../../../src/orders/mappers/productMapper');

describe(`productMapper`, () => {
  describe(`findProductsByIds`, () => {
    context(`when products with provided ids don't exist`, () => {
      it(`should return empty array`, () => {
        return findProductsByIds([99, 98])
          .then((products) => {
            expect(products).to.be.an('array').that.is.empty;
          });
      });
    });

    context(`when some products with provided ids exist`, () => {
      it(`should return array of products`, () => {
        return findProductsByIds([1, 99])
          .then((products) => {
            expect(products).to.be.an('array').that.have.lengthOf(1);

            expect(products[0]).to.be.an.instanceof(Product);
            expect(products[0].id).to.equal(1);
            expect(products[0].name).to.equal('Black sport shoes');
            expect(products[0].price).to.equal(110);
          });
      });
    });
  });

  describe(`findProductById`, () => {
    context(`when product with provided id doesn't exist`, () => {
      it(`should return null`, () => {
        return findProductById(99)
          .then((product) => {
            expect(product).to.be.null;
          });
      });
    });

    context(`when product with provided id exists`, () => {
      it(`should return instance of product`, () => {
        return findProductById(1)
          .then((product) => {
            expect(product).to.be.an.instanceof(Product);
            expect(product.id).to.equal(1);
            expect(product.name).to.equal('Black sport shoes');
            expect(product.price).to.equal(110);
          });
      });
    });
  });
});
