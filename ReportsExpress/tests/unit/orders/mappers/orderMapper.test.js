const {
  aggregateOrdersByCustomerInDay,
  aggregateOrdersByProductInDay,
} = require('../../../../src/orders/mappers/orderMapper');
const { Order } = require('../../../../src/orders/models/Order');

describe(`orderMapper`, () => {
  describe(`aggregateOrdersByCustomerInDay`, () => {
    context(`when there is no order for specific day`, () => {
      const day = `2019-08-01`;

      it(`should return empty map`, () => {
        return aggregateOrdersByCustomerInDay(day)
          .then((ordersPerCustomer) => {
            expect(ordersPerCustomer).be.an.instanceof(Map);
            expect(ordersPerCustomer).to.be.empty;
          });
      });
    });

    context(`when may customers have orders that day`, () => {
      const day = `2019-08-08`;

      it(`should return orders mapped by customer id`, () => {
        return aggregateOrdersByCustomerInDay(day)
          .then((ordersPerCustomer) => {
            expect(ordersPerCustomer).be.an.instanceof(Map).that.have.lengthOf(2);
            expect(ordersPerCustomer.get(1)).to.deep.equal([
              new Order({
                number: '2019/08/2',
                customerId: 1,
                createdAt: '2019-08-08',
                productsIds: [2],
              }),
              new Order({
                number: '2019/08/3',
                customerId: 1,
                createdAt: '2019-08-08',
                productsIds: [3],
              }),
            ]);
            expect(ordersPerCustomer.get(2)).to.deep.equal([
              new Order({
                number: '2019/08/1',
                customerId: 2,
                createdAt: '2019-08-08',
                productsIds: [1],
              }),
            ]);
          });
      });
    });
  });

  describe(`aggregateOrdersByProductInDay`, () => {
    context(`when there is no order for specific day`, () => {
      const day = `2019-08-01`;

      it(`should return empty map`, () => {
        return aggregateOrdersByProductInDay(day)
          .then((ordersPerProduct) => {
            expect(ordersPerProduct).be.an.instanceof(Map);
            expect(ordersPerProduct).to.be.empty;
          });
      });
    });

    context(`when many products was bought that day`, () => {
      const day = `2019-08-07`;

      it(`should return orders mapped by product id`, () => {
        return aggregateOrdersByProductInDay(day)
          .then((ordersPerCustomer) => {
            expect(ordersPerCustomer).be.an.instanceof(Map).that.have.lengthOf(2);
            expect(ordersPerCustomer.get(1)).to.deep.equal([
              new Order({
                number: '2019/07/1',
                customerId: 1,
                createdAt: '2019-08-07',
                productsIds: [1, 2],
              }),
              new Order({
                number: '2019/07/2',
                customerId: 2,
                createdAt: '2019-08-07',
                productsIds: [1],
              }),
            ]);
            expect(ordersPerCustomer.get(2)).to.deep.equal([
              new Order({
                number: '2019/07/1',
                customerId: 1,
                createdAt: '2019-08-07',
                productsIds: [1, 2],
              }),
            ]);
          });
      });
    });
  });
});
