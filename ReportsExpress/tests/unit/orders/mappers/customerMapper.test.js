const { Customer } = require('../../../../src/orders/models/Customer');
const { findCustomerById } = require('../../../../src/orders/mappers/customerMapper');

describe(`customerMapper`, () => {
  describe(`findCustomerById`, () => {
    context(`when customer with provided id doesn't exist`, () => {
      it(`should return null`, () => {
        return findCustomerById(99)
          .then((customer) => {
            expect(customer).to.be.null;
          });
      });
    });

    context(`when customer with provided id exists`, () => {
      it(`should return instance of customer`, () => {
        return findCustomerById(1)
          .then((customer) => {
            expect(customer).to.be.an.instanceof(Customer);

            expect(customer.id).to.equal(1);
            expect(customer.firstName).to.equal('John');
            expect(customer.lastName).to.equal('Doe');
            expect(customer.fullName).to.equal('John Doe');
          });
      });
    });
  });
});
