const supertest = require('supertest');
const app = require('../../../src/app');

describe(`GET /report/products/:date`, () => {
  context(`when there is no order for specific date`, () => {
    const date = `2019-08-20`;

    it(`it should has status code 200 and response be null`, () => {
      return supertest(app)
        .get(`/report/products/${date}`)
        .expect(200)
        .then((response) => {
          expect(response.body).to.deep.equal({ data: null });
        });
    });
  });

  context(`when there is only one product with max sell quantity`, () => {
    const date = `2019-08-07`;

    it(`it should has status code 200 and product details`, () => {
      return supertest(app)
        .get(`/report/products/${date}`)
        .expect(200)
        .then((response) => {
          expect(response.body).to.deep.equal({
            data: {
              productName: 'Black sport shoes',
              quantity: 2,
              totalPrice: 220,
            },
          });
        });
    });
  });

  context(`when there are many products with max sell quantity`, () => {
    const date = `2019-08-08`;

    it(`it should has status code 200`, () => {
      return supertest(app)
        .get(`/report/products/${date}`)
        .expect(200)
        .then((response) => {
          expect(response.body).to.deep.equal({
            data: {
              productName: 'Black sport shoes',
              quantity: 1,
              totalPrice: 110,
            },
          });
        });
    });
  });
});
