const supertest = require('supertest');
const app = require('../../../src/app');

describe(`GET /report/customer/:date`, () => {
  context(`when there is no order for specific date`, () => {
    const date = `2019-08-20`;

    it(`it should has status code 200 and return null`, () => {
      return supertest(app)
        .get(`/report/customer/${date}`)
        .expect(200)
        .then((response) => {
          expect(response.body).to.deep.equal({ data: null });
        });
    });
  });

  context(`when best buyer have multiple orders that day`, () => {
    const date = `2019-08-08`;

    it(`it should has status code 200 and return customer details`, () => {
      return supertest(app)
        .get(`/report/customer/${date}`)
        .expect(200)
        .then((response) => {
          expect(response.body).to.deep.equal({
            data: {
              customerName: 'Jane Doe',
              totalPrice: 110,
            },
          });
        });
    });
  });
});
