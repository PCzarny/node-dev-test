# Node dev tests

## Task 1
Implementation of task 1 is placed in `SortAndSearch` directory.
Bubble sort (file `ASort.ts`) and Quick sort (file `BSort.ts`) was implemented in way that both implement `ISort` interface, so can be easily replaced.

Both algorithms implements same function `swapItems`, but I decided to keep it twice separately to make those 2 classes as little binded as possible. I think that limiting unneccesary binding helps with mantaining and extending code, cause we can change one part without effect in other part

`index.ts` file contains example of usage sort and search algorithms

### Requirements
- ts-node

### Setup
```
ts-node ./SortAndSearch/index.ts
```

## Task 2
Implementation of task 2 is placed in `ReportsExpress` directory. According to provided information, I've used Express framework. As it's not defined in specification I've decided to return first bestSeller/bestBuyer that appear even if others have same result. I did this to meet provided interface, but for further development we could discuss exporting array instead of single object.

### Code organisation
Code is organized in 2 modules `orders` and `reports`. `Orders` reports contains `index` file that exports all functionality that are visible outside - only that file should be imported from other modules.

### Test convention
- controllers are tested from top to bottom - without any stubs (e2e tests)
- each methods exported from files is covered by unit tests, which stubs functionalities from other files. I assume, that not exported functions are internal part and are used to arrange code. Testing each of these will be very time consuming (development time & tests execution time) and will make it really difficult to develop in the future, so I decide only to test exported ones.

### Setup
```
cd ReportsExpress
npm i
npm start
```

### Tests
```
cd ReportsExpress
npm i
npm run test
```