import { ISort } from './ISort';

// Bubble sort
export class ASort implements ISort{
  private swapItems (arr: number[], leftIndex: number, rightIndex: number): void {
    const tmp = arr[leftIndex];
    arr[leftIndex] = arr[rightIndex];
    arr[rightIndex] = tmp
  }

  public sort (arr: number[]): number[] {
    // Create clone of input array to return sorted one without modyfing param
    // Can be skipped if mutability is acceptable
    const arrClone = [...arr];

    for (let unsortedLength = arrClone.length - 1; unsortedLength > 0; unsortedLength--) {
      let isModified = false;

      for (let itemIndex = 0; itemIndex < unsortedLength; itemIndex++) {
        if (arrClone[itemIndex] > arrClone[itemIndex + 1]) {
          isModified = true;
          this.swapItems(arrClone, itemIndex, itemIndex + 1)
        }
      }

      if (!isModified) {
        break;
      }
    }

    return arrClone;
  }
}