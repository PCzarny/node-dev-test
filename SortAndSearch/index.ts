import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

console.log(`===== Bubble sort =====`)
const bubbleSort = new ASort();
console.log(`Array sorted with Bubble sort:`);
console.log(bubbleSort.sort(unsorted));

console.log(`===== Quick sort =====`)
const quickSort = new BSort();
console.log(`Array sorted with Quick sort:`);
console.log(quickSort.sort(unsorted));

console.log(`===== Binary search =====`)
const binarySearch = BSearch.getInstance();
console.log(`Operation counter:`, BSearch.operations);

const sorted = quickSort.sort(unsorted);
console.log(`sorted array:`, sorted);

const foundIndexes = elementsToFind
    .map(elementToFind => binarySearch.find(sorted, elementToFind))

console.log(`Founded indexes:`, foundIndexes);
console.log(`Operation counter:`, BSearch.operations);
