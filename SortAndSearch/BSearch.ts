export class BSearch {
  private static instance: BSearch;
  private static operationsCount: number = 0;

  private constructor() {
  }

  static get operations(): number {
    return BSearch.operationsCount;
  }

  static getInstance(): BSearch {
    if (!BSearch.instance) {
      BSearch.instance = new BSearch();
    }

    return BSearch.instance;
  }

  private static incrementOperationsCount() {
    BSearch.operationsCount += 1;
  }

  find(sortedArray: number[], elementToFind: number): number {
    BSearch.incrementOperationsCount();

    let searchStart = 0;
    let searchEnd = sortedArray.length - 1;

    while (searchStart <= searchEnd) {
      let index = Math.floor((searchStart + searchEnd) / 2);

      if (sortedArray[index] === elementToFind) {
          return index;
      }
      if (elementToFind < sortedArray[index]) {
          searchEnd = index - 1;
      } else {
          searchStart = index + 1;
      }
    }
    return -1;
  }
}