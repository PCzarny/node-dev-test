import { ISort } from './ISort';

// QuickSort
export class BSort implements ISort{
  private swapItems = (arr: number[], leftIndex: number, rightIndex: number): void => {
    const tmp = arr[leftIndex];
    arr[leftIndex] = arr[rightIndex];
    arr[rightIndex] = tmp
  }

  private split = (array: number[], minIndex: number, maxIndex: number): number => {
    const pivot = array[maxIndex];
    let swapIndex = minIndex;

    for (let i = minIndex; i < maxIndex; i++) {
      if (array[i] < pivot) {
        this.swapItems(array, i, swapIndex);
        swapIndex += 1
      }
    }
    this.swapItems(array, swapIndex, maxIndex);
    return swapIndex;
  }

  private quickSort = (array: number[], minIndex: number, maxIndex: number): void => {
    if (minIndex < maxIndex) {
      const splitIndex = this.split(array, minIndex, maxIndex);

      this.quickSort(array, minIndex, splitIndex - 1);
      this.quickSort(array, splitIndex + 1, maxIndex);
    }
  }

  public sort = (arr: number[]): number[] => {
    // Create clone of input array to return sorted one without modyfing param
    // Can be skipped if mutability is acceptable
    const arrClone = [...arr];

    this.quickSort(arrClone, 0, arrClone.length - 1);

    return arrClone;
  }
}